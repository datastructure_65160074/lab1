public class Lab1_3 {
    public class Lab1_3_Merge_Sorted {
        public static int m, n;
    
        public static int countM(int[] nums1) {
            int m = 0;
            for (int i = 0; i < nums1.length; i++) {
                if (nums1[i] != 0) {
                    m++;
                }
            }
            return m;
        }
    
        public static int countN(int[] nums2) {
            int n = 0;
            for (int i = 0; i < nums2.length; i++) {
                if (nums2[i] != 0) {
                    n++;
                }
            }
            return n;
        }
    
        public static void removeZero_nums1(int[] nums1) {
            m = countM(nums1);
            int[] newNum1 = new int[m];
            for (int i = 0; i < nums1.length; i++) {
                if (nums1[i] != 0) {
                    newNum1[i] = nums1[i];
                }
            }
    
            for (int i = 0; i < newNum1.length; i++) {
                System.out.print(newNum1[i] + " ");
            }
            System.out.println();
        }
    
        public static void removeZero_nums2(int[] nums2) {
            n = countN(nums2);
            int[] newNum2 = new int[m];
            for (int i = 0; i < nums2.length; i++) {
                if (nums2[i] != 0) {
                    newNum2[i] = nums2[i];
                }
            }
    
            for (int i = 0; i < newNum2.length; i++) {
                System.out.print(newNum2[i] + " ");
            }
            System.out.println();
        }
    
        public static int[] createNewNum1(int[] nums1) {
            m = countM(nums1);
            int[] newNum1 = new int[m];
            for (int i = 0; i < m; i++) {
                if (nums1[i] != 0) {
                    newNum1[i] = nums1[i];
                }
            }
            return newNum1;
        }
    
        public static int[] createNewNum2(int[] nums2) {
            n = countN(nums2);
            int[] newNum2 = new int[n];
            for (int i = 0; i < m; i++) {
                if (nums2[i] != 0) {
                    newNum2[i] = nums2[i];
                }
            }
            return newNum2;
        }
    
        public static int[] createNewArray(int[] nums1, int[] nums2) {
            m = countM(nums1);
            n = countN(nums2);
            int[] newArray = new int[m + n];
            return newArray;
        }
    
        public static void MergeandSort(int[] nums1, int[] nums2) {
            int[] newArray = createNewArray(nums1, nums2);
            int[] newNum1 = createNewNum1(nums1);
            int[] newNum2 = createNewNum2(nums2);
    
            for (int i = 0, j = 0; i < newArray.length; i++, j++) {
                if (i == m) {
                    j = 0;
                }
                if (i <= m - 1) {
                    newArray[i] = newNum1[i];
                } else if (i >= n - 1) {
                    newArray[i] = newNum2[j];
                }
            }
    
            for (int i = 0; i < newArray.length; i++) {
                System.out.print(newArray[i] + " ");
            }
            System.out.println();
    
            int x = newArray.length;
            for (int i = 0; i < x - 1; i++) {
                for (int j = 0; j < x - i - 1; j++) {
                    if (newArray[j] > newArray[j + 1]) {
                        int temp = newArray[j];
                        newArray[j] = newArray[j + 1];
                        newArray[j + 1] = temp;
                    }
                }
            }
    
            System.out.print("After Sort: ");
    
            for (int i = 0; i < newArray.length; i++) {
                System.out.print(newArray[i] + " ");
            }
        }
    
        public static void main(String[] args) {
            int[] nums1 = { 1, 2, 3, 0, 0, 0 };
            int[] nums2 = { 2, 5, 6 };
    
            System.out.print("nums1 = ");
            for (int i = 0; i < nums1.length; i++) {
                System.out.print(nums1[i] + " ");
            }
            System.out.println();
    
            System.out.print("nums2 = ");
            for (int i = 0; i < nums2.length; i++) {
                System.out.print(nums2[i] + " ");
            }
            System.out.println();
    
            System.out.println("m = " + countM(nums1));
            System.out.println("n = " + countN(nums2));
    
            System.out.print("After remove 0 from nums1: ");
            removeZero_nums1(nums1);
            System.out.print("After remove 0 from nums2: ");
            removeZero_nums2(nums2);
    
            System.out.print("After merge nums1 and nums2: ");
            MergeandSort(nums1, nums2);
    
        }
    }
    
}
